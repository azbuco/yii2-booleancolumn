# Usage #

In gridview column configuration:


```
#!php
// ...
columns => [
    // ... 
    [
        'class' => azbuco\booleancolumn\BooleanColumn::class,
        'attribute' => 'isWhatever',
    ],
    // ... or
    [
        'class' => azbuco\booleancolumn\BooleanColumn::class,
        'value' => function($model) {
            return $model->status > 10 ? true : false;
        }
    ],
    // ...
]
// ...

```