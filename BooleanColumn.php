<?php

namespace azbuco\booleancolumn;

use yii\grid\DataColumn;
use yii\helpers\Html;

class BooleanColumn extends DataColumn {

    public $trueLabel = '<i class="mdi mdi-radiobox-marked text-info"></i>';
    public $falseLabel = '<i class="mdi mdi-radiobox-blank text-muted"></i>';
    public $format = 'raw';
    public $contentOptions = [
        'class' => 'text-center',
    ];
    public $headerOptions = [
        'class' => 'text-center',
    ];
    public $footerOptions = [
        'class' => 'text-center',
    ];

    public function init()
    {
        parent::init();

        Html::addCssClass($this->headerOptions, 'column-boolean');
        Html::addCssClass($this->contentOptions, 'column-boolean');
        Html::addCssClass($this->footerOptions, 'column-boolean');
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->content === null) {
            if (boolval($this->getDataCellValue($model, $key, $index)) === true) {
                return $this->trueLabel;
            }
            return $this->falseLabel;
        }

        return parent::renderDataCellContent($model, $key, $index);
    }

}
